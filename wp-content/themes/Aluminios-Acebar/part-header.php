<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse align-middle">
			<div class="small-12 medium-5 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns">
				<?php dynamic_sidebar( 'banner_top' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->
<!-- Begin Menu -->
	<section class="menu_wrap wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-10 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'info' ); ?>
			</div>
		</div>
	</section>
<!-- End Menu -->