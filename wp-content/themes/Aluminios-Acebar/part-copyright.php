<?php
if ( is_front_page() ) :
	$style = 'copyright_1';
else :
	$style = 'copyright_2';
endif;
?>
<!-- Begin Copyright -->
	<div class="copyright <?php echo $style; ?> wow fadeIn text-center" data-wow-delay="0.5s">
		&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo(name); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
	</div>
<!-- End Copyright -->