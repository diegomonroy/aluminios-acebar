<?php get_template_part( 'part', 'banner' ); ?>
<?php if ( is_front_page() ) : else : ?>
<?php $style = 'content_1'; ?>
<?php if ( is_page( array( 'nuestra-empresa' ) ) ) : $style = 'content_1'; endif; ?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row container">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->
<?php endif; ?>